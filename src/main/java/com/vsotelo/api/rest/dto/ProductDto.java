package com.vsotelo.api.rest.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class ProductDto {

    private Integer correlative;
    private Integer id;
    private String name;
    private String status;
    private String category;
    private Double price;

    @JsonIgnore
    private Integer total;

}
