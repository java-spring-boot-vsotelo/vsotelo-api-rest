package com.vsotelo.api.rest.dto;

import lombok.Data;

@Data
public class UserDto {

    private Integer userId;
    private String name;
    private String nickName;
    private String password;
}
