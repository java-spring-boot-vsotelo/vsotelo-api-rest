package com.vsotelo.api.rest.dao;

import com.vsotelo.api.rest.dto.UserDto;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.dao.DataAccessException;

import java.util.List;

@Mapper
public interface UserMapper {

    UserDto findById(Integer userId) throws DataAccessException;

    List<UserDto> findAll() throws DataAccessException;

    List<UserDto> findAllSP(Integer userId) throws DataAccessException;

}
