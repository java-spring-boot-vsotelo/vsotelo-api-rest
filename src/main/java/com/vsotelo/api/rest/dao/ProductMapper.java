package com.vsotelo.api.rest.dao;

import com.vsotelo.api.rest.dto.ProductDto;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.dao.DataAccessException;

import java.util.HashMap;
import java.util.List;

@Mapper
public interface ProductMapper {

    List<ProductDto> callProductPagination(HashMap<String, Object> params) throws DataAccessException;

}
