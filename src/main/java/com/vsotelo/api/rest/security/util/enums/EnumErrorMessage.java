package com.vsotelo.api.rest.security.util.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum EnumErrorMessage implements ErrorMessage {

    RESPONSE_ERROR_UNAUTHORIZED("RS001", "Unauthorized user!"),
    ERROR_GENERIC("RS002", "Error Generic"),
    RESPONSE_ERROR_SIGNATURE("RS003", "Signature not match"),
    EXCEPTION_SQL("ESQL001", "Error in sql query"),
    ERROR_PARSE_CLAIM("RS004", "Error in parsing claim values");

    private String code;
    private String message;

}