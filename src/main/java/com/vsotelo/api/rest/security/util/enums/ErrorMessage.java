package com.vsotelo.api.rest.security.util.enums;

public interface ErrorMessage {

    String getCode();

    String getMessage();
}
