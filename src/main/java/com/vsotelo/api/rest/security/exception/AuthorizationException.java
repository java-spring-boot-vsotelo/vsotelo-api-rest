package com.vsotelo.api.rest.security.exception;

import lombok.Data;

@Data
public class AuthorizationException  extends Exception {
    private static final long serialVersionUID = 1L;

    private String message;

    public AuthorizationException(String message) {
        super();
        this.message = message;
    }
}