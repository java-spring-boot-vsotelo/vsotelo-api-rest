package com.vsotelo.api.rest.security.util.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Claims {
    USER_CODE("userCode", "userCode"),
    USER_NAME("userName", "userName"),
    USER_EMAIL("userEmail", "userEmail"),
    USER_POSITION("userPosition", "userPosition"),
    USER_PROFILE("userProfile", "userProfile");

    private String name;
    private String value;
}
