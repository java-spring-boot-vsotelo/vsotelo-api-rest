package com.vsotelo.api.rest.security.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClaimBody {

    private String userCode;
    private String userName;
    private String userEmail;
    private String userPosition;
    private String userProfile;
    private boolean tokenExpired;
}
