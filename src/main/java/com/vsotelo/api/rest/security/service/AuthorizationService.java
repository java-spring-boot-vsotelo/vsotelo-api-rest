package com.vsotelo.api.rest.security.service;

import com.vsotelo.api.rest.security.dto.ClaimBody;
import com.vsotelo.api.rest.security.expose.req.TokenRq;
import org.springframework.http.ResponseEntity;

public interface AuthorizationService {
    ResponseEntity<ClaimBody> token(TokenRq request);
}
