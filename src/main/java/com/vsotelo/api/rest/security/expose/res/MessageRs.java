package com.vsotelo.api.rest.security.expose.res;

import lombok.Data;

@Data
public class MessageRs {

    private String code;
    private String message;

}
