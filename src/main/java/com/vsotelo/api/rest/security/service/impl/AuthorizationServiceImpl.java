package com.vsotelo.api.rest.security.service.impl;

import com.vsotelo.api.rest.security.dto.ClaimBody;
import com.vsotelo.api.rest.security.expose.req.TokenRq;
import com.vsotelo.api.rest.security.service.AuthorizationService;
import com.vsotelo.api.rest.security.util.JsonWebTokenUtil;
import io.jsonwebtoken.MalformedJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class AuthorizationServiceImpl implements AuthorizationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizationServiceImpl.class);

    @Override
    public ResponseEntity<ClaimBody> token(TokenRq request) {

        JsonWebTokenUtil jwtUtil = new JsonWebTokenUtil();
        try {
            return ResponseEntity.ok(jwtUtil.decodeJWTToken(request.getToken()));
        } catch (MalformedJwtException e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.ok(new ClaimBody());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.ok(new ClaimBody());
        }
    }
}
