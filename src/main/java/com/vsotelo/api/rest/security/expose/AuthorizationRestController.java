package com.vsotelo.api.rest.security.expose;


import com.vsotelo.api.rest.security.expose.req.TokenRq;
import com.vsotelo.api.rest.security.dto.*;
import com.vsotelo.api.rest.security.service.AuthorizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/token")
public class AuthorizationRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizationRestController.class);

    @Autowired
    AuthorizationService authorizationService;

    @PostMapping("/validation")
    public ResponseEntity<ClaimBody> tokenValidation(@RequestBody TokenRq request) {
        return authorizationService.token(request);
    }

}
