package com.vsotelo.api.rest.security.expose.req;

import lombok.Data;

@Data
public class TokenRq {
    private String token;
}
