package com.vsotelo.api.rest.security.service.impl;


import com.vsotelo.api.rest.security.exception.AuthorizationException;
import com.vsotelo.api.rest.security.dto.ClaimBody;
import com.vsotelo.api.rest.security.service.UserLoginService;
import com.vsotelo.api.rest.security.util.enums.EnumErrorMessage;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserLoginServiceImpl implements UserLoginService {

    @Override
    public Optional<ClaimBody> searchUserByGlobalId(String user, String password) throws AuthorizationException {
        return Optional.of(getUserLasted(user, password));
    }

    private static ClaimBody getUserLasted(String user, String password) throws AuthorizationException {
        if (user.equals("vsotelo")) {
            if (password.equals("vsotelo")) {
                return new ClaimBody(user, "vs", "mail@mail.com", "admin", "admin", true);
            } else {
                throw new AuthorizationException(EnumErrorMessage.EXCEPTION_SQL.getMessage());
            }
        } else {
            throw new AuthorizationException(EnumErrorMessage.EXCEPTION_SQL.getMessage());
        }
    }
}
