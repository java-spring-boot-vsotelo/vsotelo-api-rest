package com.vsotelo.api.rest.security.service;

import com.vsotelo.api.rest.security.exception.AuthorizationException;
import com.vsotelo.api.rest.security.dto.ClaimBody;

import java.util.Optional;

public interface UserLoginService {
    Optional<ClaimBody> searchUserByGlobalId(String user, String password) throws AuthorizationException;
}
