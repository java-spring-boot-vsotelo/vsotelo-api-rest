package com.vsotelo.api.rest.security.service.impl;

import com.google.gson.Gson;
import com.vsotelo.api.rest.security.exception.AuthorizationException;
import com.vsotelo.api.rest.security.dto.ClaimBody;
import com.vsotelo.api.rest.security.service.UserLoginService;
import com.vsotelo.api.rest.security.util.SecurityConstants;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class LoginAuthenticationProvider implements AuthenticationProvider {

    private static final Logger LOG = LoggerFactory.getLogger(LoginAuthenticationProvider.class);
    private static final Gson gson = new Gson();

    private final UserLoginService userLoginService;


    @Autowired
    public LoginAuthenticationProvider(UserLoginService userLoginService) {
        this.userLoginService = userLoginService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        ClaimBody claimBody = new ClaimBody();
        Optional<ClaimBody> userOptional = Optional.empty();
        String username = authentication.getName();
        String password = Optional.ofNullable(authentication.getCredentials()).orElse(StringUtils.EMPTY).toString();

        try {
            userOptional = userLoginService.searchUserByGlobalId(username, password);
        } catch (AuthorizationException e) {
            LOG.error("ERROR IN GET USER OF USERNAME" + e.getMessage(), e);
        }
        if (userOptional.isPresent()) {
            LOG.debug("User Recovery: " + gson.toJson(userOptional));
            claimBody.setUserCode(StringUtils.trim(userOptional.get().getUserCode()));
            claimBody.setUserName(StringUtils.trim(userOptional.get().getUserName()));
            claimBody.setUserEmail(StringUtils.trim(userOptional.get().getUserEmail()));
            claimBody.setUserPosition(StringUtils.trim(userOptional.get().getUserPosition()));
            claimBody.setUserProfile(StringUtils.trim(userOptional.get().getUserProfile()));
        } else {
            LOG.error("[SCI] EMPLOYEE NULL");
            throw new BadCredentialsException("ERROR IN VALIDATION OF USER");
        }

        List<GrantedAuthority> grantedAuths = new ArrayList<>();
        grantedAuths.add(new SimpleGrantedAuthority(SecurityConstants.ROL_ADMIN));
        Object details = gson.toJson(claimBody);

        LOG.debug("Obtaining user data : " + gson.toJson(claimBody));

        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(username, password, grantedAuths);
        auth.setDetails(details);
        return auth;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UsernamePasswordAuthenticationToken.class);
    }
}
