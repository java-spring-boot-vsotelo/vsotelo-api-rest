package com.vsotelo.api.rest.security.dto;

import lombok.Data;

@Data
public class TokenExpiration {
    private String code;
    private String message;
}
