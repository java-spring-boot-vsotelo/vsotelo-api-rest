package com.vsotelo.api.rest.security.filter;

import java.io.IOException;
import java.util.*;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vsotelo.api.rest.security.dto.ClaimBody;
import com.vsotelo.api.rest.security.expose.res.TokenRs;
import com.vsotelo.api.rest.security.util.JsonWebTokenUtil;
import com.vsotelo.api.rest.security.util.SecurityConstants;
import com.vsotelo.api.rest.security.util.enums.Claims;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    private final Log LOGGER = LogFactory.getLog(this.getClass());

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse res = (HttpServletResponse) response;
        ClaimBody claims = new ClaimBody();
        String uri = req.getRequestURI();

        // access swagger api docs or /token/validation
        if (uri.contains("swagger") || uri.contains("api-docs") || uri.contains("health.html") || uri.contains("token")) {
            LOGGER.debug("Access to swagger or validation token controller");

            claims.setUserCode(StringUtils.EMPTY);
            claims.setUserName(StringUtils.EMPTY);
            claims.setUserEmail(StringUtils.EMPTY);
            claims.setUserPosition(StringUtils.EMPTY);
            claims.setUserProfile(StringUtils.EMPTY);
            claims.setTokenExpired(false);

            setterHeader(claims, res, req);
            chain.doFilter(req, res);

            return;
        }

        String authorization = SecurityConstants.AUTHORIZATION;
        final String authHeader = Optional.ofNullable(req.getHeader(authorization)).orElse(StringUtils.EMPTY);

        // token is empty
        if (authHeader.isEmpty() && !uri.contains("swagger") && !uri.contains("api-docs")) {
            LOGGER.debug("Credentials needed or Invalid token");
            claims.setUserCode(StringUtils.EMPTY);
            claims.setUserName(StringUtils.EMPTY);
            claims.setUserEmail(StringUtils.EMPTY);
            claims.setUserPosition(StringUtils.EMPTY);
            claims.setUserProfile(StringUtils.EMPTY);
            claims.setTokenExpired(false);

            Map<String, String> data = new HashMap<>(3);

            data.put(SecurityConstants.CODE, SecurityConstants.CODE_ERROR_UNAUTHORIZED);
            data.put(SecurityConstants.MESSAGE, SecurityConstants.INVALID_USER_CREDENTIALS);
            data.put(SecurityConstants.CAUSE, SecurityConstants.CREDENTIALS_NEEDED_INVALID_TOKEN);

            res.setContentType(SecurityConstants.APPLICATION_JSON);
            res.setCharacterEncoding(SecurityConstants.UTF8);
            res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            res.getWriter().write(new JSONObject(data).toString());
            res.getWriter().flush();
            return;
        }

        // token is not empty
        if (!authHeader.isEmpty() && !uri.contains("swagger") && !uri.contains("api-docs")) {
            TokenRs tokenRs = new TokenRs();
            tokenRs.setToken(authHeader);
            //class util for decode jwt token
            JsonWebTokenUtil jwtUtil = new JsonWebTokenUtil();
            claims = jwtUtil.decodeJWTToken(authHeader);

            if (claims != null) {
                if (claims.isTokenExpired()) {
                    LOGGER.debug("Expired Token!!");
                    Map<String, String> data = new HashMap<>(3);
                    data.put(SecurityConstants.CODE, SecurityConstants.CODE_ERROR_UNAUTHORIZED);
                    data.put(SecurityConstants.MESSAGE, SecurityConstants.RESPONSE_ERROR_EXPIRED_TOKEN);
                    data.put(SecurityConstants.CAUSE, SecurityConstants.EXPIRED_TOKEN);
                    setterHeader(claims, res, req);
                    res.setContentType(SecurityConstants.APPLICATION_JSON);
                    res.setCharacterEncoding(SecurityConstants.UTF8);
                    res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    res.getWriter().write(new JSONObject(data).toString());
                    res.getWriter().flush();
                    return;
                } else if (StringUtils.isBlank(claims.getUserCode())) {
                    LOGGER.debug("Invalid user credentials");
                    Map<String, String> data = new HashMap<>(3);
                    data.put(SecurityConstants.CODE, SecurityConstants.CODE_ERROR_UNAUTHORIZED);
                    data.put(SecurityConstants.MESSAGE, SecurityConstants.INVALID_USER_CREDENTIALS);
                    data.put(SecurityConstants.CAUSE, SecurityConstants.CREDENTIALS_NEEDED_INVALID_TOKEN);

                    setterHeader(claims, res, req);

                    res.setContentType(SecurityConstants.APPLICATION_JSON);
                    res.setCharacterEncoding(SecurityConstants.UTF8);
                    res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    res.getWriter().write(new JSONObject(data).toString());
                    res.getWriter().flush();
                    return;
                } else {
                    // get context authentication
                    Authentication authentication = jwtUtil.getAuthentication((HttpServletRequest) request, (HttpServletResponse) response);
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                    setterHeader(claims, res, req);
                    chain.doFilter(request, response);
                }
            } else {
                LOGGER.debug("Invalid user credentials");
                Map<String, String> data = new HashMap<>(3);
                data.put(SecurityConstants.CODE, SecurityConstants.CODE_ERROR_UNAUTHORIZED);
                data.put(SecurityConstants.MESSAGE, SecurityConstants.INVALID_USER_CREDENTIALS);
                data.put(SecurityConstants.CAUSE, SecurityConstants.CREDENTIALS_NEEDED_INVALID_TOKEN);
                res.setContentType(SecurityConstants.APPLICATION_JSON);
                res.setCharacterEncoding(SecurityConstants.UTF8);
                res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                res.getWriter().write(new JSONObject(data).toString());
                res.getWriter().flush();
                return;
            }
        }
    }

    private void setterHeader(ClaimBody claims, HttpServletResponse res, HttpServletRequest req) {
        //Setter response
        res.setContentType(SecurityConstants.APPLICATION_JSON);
        res.setCharacterEncoding(SecurityConstants.UTF8);
        res.setStatus(HttpServletResponse.SC_OK);
        res.addHeader(Claims.USER_CODE.getValue(), claims.getUserCode());
        res.addHeader(Claims.USER_NAME.getValue(), claims.getUserName());
        res.addHeader(Claims.USER_EMAIL.getValue(), claims.getUserEmail());
        res.addHeader(Claims.USER_POSITION.getValue(), claims.getUserPosition());
        res.addHeader(Claims.USER_PROFILE.getValue(), claims.getUserProfile());
        //Setter response
        res.setCharacterEncoding(SecurityConstants.UTF8);
        req.setAttribute(Claims.USER_CODE.getValue(), claims.getUserCode());
        req.setAttribute(Claims.USER_NAME.getValue(), claims.getUserName());
        req.setAttribute(Claims.USER_EMAIL.getValue(), claims.getUserEmail());
        req.setAttribute(Claims.USER_POSITION.getValue(), claims.getUserPosition());
        req.setAttribute(Claims.USER_PROFILE.getValue(), claims.getUserProfile());
    }
}