package com.vsotelo.api.rest.security.dto;

import lombok.Data;

@Data
public class UserCredentialsDto {

    private String username;
    private String password;

}
