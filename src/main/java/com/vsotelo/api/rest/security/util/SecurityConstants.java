package com.vsotelo.api.rest.security.util;

public class SecurityConstants {

    public static final String UTF8 = "UTF-8";
    public static final String APPLICATION_JSON = "application/json";
    public static final String AUTHORIZATION = "Authorization";
    public static final String API_KEY = "api-key";
    public static final String DENY = "Deny";
    public static final String ROL_ADMIN = "ADMIN";
    public static final String KEY_USER = "userCode";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_KEY = "Authorization";
    public static final String MESSAGE_TOKEN_EXPIRED = "expired token";
    public final static String LOGGER_FORMAT_DATE = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final String CODE = "code";
    public static final String MESSAGE = "message";
    public static final String CAUSE = "cause";
    public static final String CODE_ERROR_UNAUTHORIZED = "CRU001";
    public static final String INVALID_USER_CREDENTIALS = "Invalid user credentials";
    public static final String CREDENTIALS_NEEDED_INVALID_TOKEN = "credentials needed or Invalid token";
    public static final String RESPONSE_ERROR_EXPIRED_TOKEN = "Unauthorized user!";
    public static final String EXPIRED_TOKEN = "expired token";
}
