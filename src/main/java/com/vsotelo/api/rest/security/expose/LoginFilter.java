package com.vsotelo.api.rest.security.expose;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vsotelo.api.rest.security.expose.res.MessageRs;
import com.vsotelo.api.rest.security.dto.UserCredentialsDto;
import com.vsotelo.api.rest.security.util.JsonWebTokenUtil;
import com.vsotelo.api.rest.security.util.SecurityConstants;
import com.vsotelo.api.rest.security.util.enums.EnumErrorMessage;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Collections;

public class LoginFilter extends AbstractAuthenticationProcessingFilter {

    private static final Logger LOG = LoggerFactory.getLogger(LoginFilter.class);
    private static final Gson GSON = new GsonBuilder().serializeNulls().setDateFormat(SecurityConstants.LOGGER_FORMAT_DATE).create();

    public LoginFilter(String url, AuthenticationManager authManager) {
        super(new AntPathRequestMatcher(url));
        setAuthenticationManager(authManager);
    }


    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {
        InputStream body = httpServletRequest.getInputStream();
        UserCredentialsDto user = new ObjectMapper().readValue(body, UserCredentialsDto.class);
        return getAuthenticationManager().authenticate(new UsernamePasswordAuthenticationToken(user.getUsername(),
                user.getPassword(), Collections.emptyList()));
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication auth) {
        LOG.debug("toke generated...");
        JsonWebTokenUtil jwtUtil = new JsonWebTokenUtil();
        try {
            jwtUtil.addAuthentication(response, auth.getName(), auth.getDetails());
        } catch (Exception e) {
            LOG.error("Error Authentication: " + e.getMessage(), e);
        }
    }

    @Override
    public void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException {
        MessageRs messageRs = new MessageRs();
        messageRs.setCode(EnumErrorMessage.RESPONSE_ERROR_UNAUTHORIZED.getCode());
        messageRs.setMessage(EnumErrorMessage.RESPONSE_ERROR_UNAUTHORIZED.getMessage());
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setHeader(SecurityConstants.AUTHORIZATION, SecurityConstants.DENY);
        PrintWriter out = response.getWriter();
        response.setContentType(SecurityConstants.APPLICATION_JSON);
        response.setCharacterEncoding(SecurityConstants.UTF8);
        out.write(GSON.toJson(messageRs));
        out.flush();
        IOUtils.closeQuietly(out);
    }
}
