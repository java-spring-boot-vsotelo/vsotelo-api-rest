package com.vsotelo.api.rest.security.expose.res;

import lombok.Data;

@Data
public class TokenRs {

    private String token;

}
