package com.vsotelo.api.rest.security.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vsotelo.api.rest.security.exception.AuthorizationException;
import com.vsotelo.api.rest.security.dto.ClaimBody;
import com.vsotelo.api.rest.security.dto.TokenExpiration;
import com.vsotelo.api.rest.security.expose.res.TokenRs;
import com.vsotelo.api.rest.security.util.enums.EnumErrorMessage;
import io.jsonwebtoken.*;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component("JsonWebTokenUtil")
public class JsonWebTokenUtil {

    private static final Logger LOG = LoggerFactory.getLogger(JsonWebTokenUtil.class);
    private static final Gson GSON = new GsonBuilder().serializeNulls().setDateFormat(SecurityConstants.LOGGER_FORMAT_DATE).create();

    @Value("${jwt.secret}")
    private String SECRET_KEY;

    @Value("${jwt.expiration}")
    private Long EXPIRY_TIME;

    static private String secretKey;
    static private Long expiryTime;

    @PostConstruct
    public void setValue() {
        secretKey = SECRET_KEY;
        expiryTime = EXPIRY_TIME;
    }

    public JsonWebTokenUtil() {
        String v = StringUtils.EMPTY;
        LOG.debug("Inside of JwtUtil constructor" + v);
    }
    public void addAuthentication(HttpServletResponse res, String username, Object details) throws AuthorizationException {
        String det = (String) details;
        ObjectMapper mapper = new ObjectMapper();
        ClaimBody body;
        Map<String, Object> claims = new HashMap<>();
        try {
            body = mapper.readValue(det, ClaimBody.class);
            claims.put("userCode", StringUtils.trimToEmpty(body.getUserCode()));
            claims.put("userName", StringUtils.trimToEmpty(body.getUserName()));
            claims.put("userEmail", StringUtils.trimToEmpty(body.getUserEmail()));
            claims.put("userPosition", StringUtils.trimToEmpty(body.getUserPosition()));
            claims.put("userProfile", StringUtils.trimToEmpty(body.getUserProfile()));

        } catch (Exception e) {
            LOG.error("Error addAuthentication: " + e.getMessage(), e);
            throw new AuthorizationException(EnumErrorMessage.ERROR_PARSE_CLAIM.getMessage());
        }

        Date now = new Date();
        Date validity = new Date(now.getTime() + expiryTime);
        try {
            String token = Jwts.builder()
                    .setSubject(username)
                    .setClaims(claims)
                    .setIssuedAt(now)
                    .setExpiration(validity)
                    .signWith(SignatureAlgorithm.HS512, secretKey).compact();

            res.addHeader(SecurityConstants.HEADER_KEY, SecurityConstants.TOKEN_PREFIX.concat(token));
            TokenRs tokenRs = new TokenRs();
            PrintWriter out;
            tokenRs.setToken(SecurityConstants.TOKEN_PREFIX.concat(token));

            out = res.getWriter();
            res.setContentType(SecurityConstants.APPLICATION_JSON);
            res.setCharacterEncoding(SecurityConstants.UTF8);


            if (out != null) {
                out.print(GSON.toJson(tokenRs));
                out.flush();
                IOUtils.closeQuietly(out);
            }
        } catch (Exception e) {
            LOG.error("Error addAuthentication: ");
            throw new AuthorizationException(EnumErrorMessage.ERROR_GENERIC.getMessage());
        }
    }

    public Authentication getAuthentication(HttpServletRequest request, HttpServletResponse response) {
        String token = request.getHeader(SecurityConstants.HEADER_KEY);
        TokenExpiration tokenExpiration = new TokenExpiration();
        Jws<Claims> claims;
        if (token != null) {
            try {
                claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(StringUtils.trim(token.replace(SecurityConstants.TOKEN_PREFIX, StringUtils.EMPTY)));
                if (claims.getBody().getExpiration().before(new Date())) {
                    generateJsonTokenExpired(response, tokenExpiration);
                }
                String user = claims.getBody().get(SecurityConstants.KEY_USER).toString();
                return user != null ? new UsernamePasswordAuthenticationToken(Collections.emptyList(), user, null) : null;
            } catch (Exception e) {
                generateJsonTokenExpired(response, tokenExpiration);
                return new UsernamePasswordAuthenticationToken(Collections.emptyList(), null);
            }
        }
        return null;
    }

    private void generateJsonTokenExpired(HttpServletResponse response, TokenExpiration tokenExpiration) {

        tokenExpiration.setCode(StringUtils.trim(EnumErrorMessage.RESPONSE_ERROR_UNAUTHORIZED.getCode()));
        tokenExpiration.setMessage(SecurityConstants.MESSAGE_TOKEN_EXPIRED);

        PrintWriter out = null;
        try {
            out = response.getWriter();

            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response.setHeader(SecurityConstants.AUTHORIZATION, SecurityConstants.DENY);
            response.setContentType(SecurityConstants.APPLICATION_JSON);
            response.setCharacterEncoding(SecurityConstants.UTF8);

            if (out != null) {
                out.print(GSON.toJson(tokenExpiration));
                out.flush();
                IOUtils.closeQuietly(out);
            }

        } catch (IOException e) {
            LOG.error("Error IOException: " + e.getMessage(), e);
        }
    }

    public ClaimBody decodeJWTToken(String token) {
        try {
            Claims claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(StringUtils.trim(token.replace(SecurityConstants.TOKEN_PREFIX, StringUtils.EMPTY))).getBody();
            ClaimBody body = new ClaimBody();
            body.setUserCode(MapUtils.getString(claims, "userCode", StringUtils.EMPTY).trim());
            body.setUserName(MapUtils.getString(claims, "userName", StringUtils.EMPTY).trim());
            body.setUserEmail(MapUtils.getString(claims, "userEmail", StringUtils.EMPTY).trim());
            body.setUserPosition(MapUtils.getString(claims, "userPosition", StringUtils.EMPTY).trim());
            body.setUserProfile(MapUtils.getString(claims, "userProfile", StringUtils.EMPTY).trim());
            body.setTokenExpired(false);
            return body;
        } catch (ExpiredJwtException e) {
            LOG.error("ExpiredJwtException: " + e.getMessage(), e);
            ClaimBody body = new ClaimBody();
            body.setUserCode(StringUtils.EMPTY);
            body.setUserName(StringUtils.EMPTY);
            body.setUserEmail(StringUtils.EMPTY);
            body.setUserPosition(StringUtils.EMPTY);
            body.setUserProfile(StringUtils.EMPTY);
            body.setTokenExpired(true);
            return body;
        } catch (Exception e) {
            LOG.error("SignatureException: " + e.getMessage(), e);
            ClaimBody body = new ClaimBody();
            body.setUserCode(StringUtils.EMPTY);
            body.setUserName(StringUtils.EMPTY);
            body.setUserEmail(StringUtils.EMPTY);
            body.setUserPosition(StringUtils.EMPTY);
            body.setUserProfile(StringUtils.EMPTY);
            body.setTokenExpired(true);
            return body;
        }
    }

}
