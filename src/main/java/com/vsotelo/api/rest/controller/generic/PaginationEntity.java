package com.vsotelo.api.rest.controller.generic;

import lombok.Data;

import java.util.List;

@Data
public class PaginationEntity<E> {

    private List<E> _results;
    private _links _links;
    private _page _page;


    @Data
    public static class _links {
        private String _first;
        private String _self;
        private String next;
        private String last;
    }

    @Data
    public static class _page {
        private Integer _size;
        private Integer _total_elements;
        private Integer _total_pages;
    }
}
