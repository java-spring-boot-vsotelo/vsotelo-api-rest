package com.vsotelo.api.rest.controller;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vsotelo.api.rest.controller.generic.ObjectResponse;
import com.vsotelo.api.rest.dto.UserDto;
import com.vsotelo.api.rest.security.util.SecurityConstants;
import com.vsotelo.api.rest.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.catalina.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/user")
@Tag(name = "User", description = "The User API")
@SecurityRequirement(name = "api-key")
public class UserRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserRestController.class);
    private static final Gson GSON = new GsonBuilder().serializeNulls().setDateFormat(SecurityConstants.LOGGER_FORMAT_DATE).create();

    private final UserService userService;
    private final HttpServletRequest httpServletRequest;

    @Autowired
    public UserRestController(UserService userService, HttpServletRequest httpServletRequest) {
        this.userService = userService;
        this.httpServletRequest = httpServletRequest;
    }

    @Operation(summary = "Search User", description = "Search User by Id.", tags = {"User"})
    @ApiResponses(value = {@ApiResponse(description = "Successful operation", responseCode = "CVS0001", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = User.class))})})
    @GetMapping(value = "/find/{id}")
    public ResponseEntity<ObjectResponse<UserDto>> findById(@RequestParam(value = "id") Integer id) {
        LOGGER.info("[controller] /user/find/{id}, request ->  {}", id);
        long starTime = System.currentTimeMillis();
        ObjectResponse<UserDto> response = new ObjectResponse<>();
        HttpStatus httpStatus;
        UserDto userDto = userService.findById(id);
        try {
            if (userDto != null) {
                response.setResult(userDto);
                response.setMessage("Successful operation");
                httpStatus = HttpStatus.OK;
            } else {
                response.setResult(null);
                response.setMessage("No records");
                httpStatus = HttpStatus.NO_CONTENT;
            }
        } catch (Exception e) {
            response.setResult(null);
            response.setMessage(e.getMessage());
            response.setException(e.getClass().getName());
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        response.setStatusCode(httpStatus.value());
        response.setMethod("findById()");
        response.setServiceUrl(httpServletRequest.getRequestURI());

        long endTime = System.currentTimeMillis();
        LOGGER.info("[task] Duration of execution (ms): {}", (endTime - starTime));
        return new ResponseEntity<>(response, httpStatus);
    }

}