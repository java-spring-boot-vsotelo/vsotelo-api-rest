package com.vsotelo.api.rest.controller.generic;

import lombok.Data;

import java.io.Serializable;

@Data
public class ObjectResponse<R> implements Serializable {

    private Integer statusCode;
    private String message;
    private String serviceUrl;
    private String method;
    private String exception;
    private R result;
}
