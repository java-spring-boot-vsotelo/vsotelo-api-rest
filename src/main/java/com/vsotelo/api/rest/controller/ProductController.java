package com.vsotelo.api.rest.controller;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vsotelo.api.rest.controller.generic.ObjectResponse;
import com.vsotelo.api.rest.controller.generic.PaginationEntity;
import com.vsotelo.api.rest.dto.ProductDto;
import com.vsotelo.api.rest.security.util.SecurityConstants;
import com.vsotelo.api.rest.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/product")
@Tag(name = "product", description = "The Product API")
@SecurityRequirement(name = "api-key")
public class ProductController {


    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);
    private static final Gson GSON = new GsonBuilder().serializeNulls().setDateFormat(SecurityConstants.LOGGER_FORMAT_DATE).create();

    private final ProductService productService;
    private final HttpServletRequest httpServletRequest;

    @Autowired
    public ProductController(ProductService productService, HttpServletRequest httpServletRequest) {
        this.productService = productService;
        this.httpServletRequest = httpServletRequest;
    }


    @Operation(summary = "All Products by pagination", description = "Search Products", tags = {"Product"})
    @ApiResponses(value = {@ApiResponse(description = "Successful operation", responseCode = "CVS0001", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ProductDto.class))})})
    @GetMapping()
    public ResponseEntity<?> findById(@RequestParam int page, @RequestParam int size) {
        LOGGER.info("[controller]");
        long starTime = System.currentTimeMillis();
        ObjectResponse<PaginationEntity<ProductDto>> response = new ObjectResponse<>();
        HttpStatus httpStatus;
        List<ProductDto> productDto = productService.callProductPagination(size, 1, 20);
        try {
            if (productDto != null) {
                PaginationEntity<ProductDto> productPaginationEntity = new PaginationEntity<>();
                PaginationEntity._page pageEntity = new PaginationEntity._page();
                pageEntity.set_size(20);
                pageEntity.set_total_elements(productDto.get(0).getTotal());
                pageEntity.set_total_pages(productDto.get(0).getTotal() / 20);
                PaginationEntity._links links = new PaginationEntity._links();

                productPaginationEntity.set_results(productDto);
                productPaginationEntity.set_links(links);
                productPaginationEntity.set_page(pageEntity);

                response.setResult(productPaginationEntity);
                response.setMessage("Successful operation");
                httpStatus = HttpStatus.OK;
            } else {
                response.setResult(null);
                response.setMessage("No records");
                httpStatus = HttpStatus.NO_CONTENT;
            }
        } catch (Exception e) {
            response.setResult(null);
            response.setMessage(e.getMessage());
            response.setException(e.getClass().getName());
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        response.setStatusCode(httpStatus.value());
        response.setMethod("findById()");
        response.setServiceUrl(httpServletRequest.getRequestURI());

        long endTime = System.currentTimeMillis();
        LOGGER.info("[task] Duration of execution (ms): {}", (endTime - starTime));
        return new ResponseEntity<>(response, httpStatus);
    }

}
