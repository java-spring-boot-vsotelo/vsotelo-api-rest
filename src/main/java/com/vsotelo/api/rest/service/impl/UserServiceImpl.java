package com.vsotelo.api.rest.service.impl;

import com.vsotelo.api.rest.dao.UserMapper;
import com.vsotelo.api.rest.dto.UserDto;
import com.vsotelo.api.rest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    @Override
    public UserDto findById(Integer userId) {
        return userMapper.findById(userId);
    }

    @Override
    public List<UserDto> findAll() {
        return userMapper.findAll();
    }

    @Override
    public List<UserDto> findAllSP(Integer userId) {
        return userMapper.findAllSP(userId);
    }
}
