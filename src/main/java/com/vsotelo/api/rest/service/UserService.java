package com.vsotelo.api.rest.service;

import com.vsotelo.api.rest.dto.UserDto;

import java.util.List;

public interface UserService {

    UserDto findById(Integer userId);
    List<UserDto> findAll();
    List<UserDto> findAllSP(Integer userId);
}
