package com.vsotelo.api.rest.service.impl;

import com.vsotelo.api.rest.dao.ProductMapper;
import com.vsotelo.api.rest.dto.ProductDto;
import com.vsotelo.api.rest.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductMapper productMapper;

    @Autowired
    public ProductServiceImpl(ProductMapper productMapper) {
        this.productMapper = productMapper;
    }

    @Override
    public List<ProductDto> callProductPagination(int limit, int from, int to) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("limit", limit);
        params.put("from", from);
        params.put("to", to);
        return productMapper.callProductPagination(params);
    }
}
