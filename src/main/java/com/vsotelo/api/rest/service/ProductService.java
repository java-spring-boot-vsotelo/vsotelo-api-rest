package com.vsotelo.api.rest.service;

import com.vsotelo.api.rest.dto.ProductDto;

import java.util.List;

public interface ProductService {

    List<ProductDto> callProductPagination(int limit, int from, int to);
}
