FROM openjdk:8-jdk-slim
COPY "./target/vsotelo-api-rest-1.0-SNAPSHOT.jar" "vsotelo-api-rest.jar"
EXPOSE 8080
ENTRYPOINT ["java","-jar","vsotelo-api-rest.jar"]
